#  Debian-local repository

This repository contains a debian repository available at `https://protocole.gitlab.io/debian-repo/debian-local/`. Packages available in this are automatically converted into `.deb` files and are available to use in the link given upper.

## How to use 

Add gpg key 

```
curl -fsSL https://protocole.gitlab.io/debian-repo/debian-local/keyring.gpg | sudo gpg --dearmor -o /usr/share/keyrings/protocole-archive-keyring.gpg

```

Add this repository into your sources:

```
echo \
  "deb [signed-by=/usr/share/keyrings/protocole-archive-keyring.gpg] https://protocole.gitlab.io/debian-repo/debian-local ./" | sudo tee /etc/apt/sources.list.d/protocole.list > /dev/null
```

